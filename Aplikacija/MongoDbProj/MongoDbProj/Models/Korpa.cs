﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDbProj.Models
{
    public class Korpa
    {
        public ObjectId Id { get; set; }

        public ObjectId Korisnik { get; set; }
        public List<Proizvod> Proizvodi { get; set; }

        public float UkupnaCenaKorpe { get; set; }

        public Korpa()
        {
            Proizvodi = new List<Proizvod>();
        }
    }
}

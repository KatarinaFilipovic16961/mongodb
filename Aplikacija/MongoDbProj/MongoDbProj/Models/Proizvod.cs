﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDbProj.Models
{
    public class Proizvod
    {
        public ObjectId Id { get; set; }
        public string Sifra { get; set; }
        public string Naziv { get; set; }
        public float Cena { get; set; }
        public int Kolicina { get;set;}
        public string IdProdavnice { get; set; }

        public ObjectId prodavnica { get; set; }
    }
}

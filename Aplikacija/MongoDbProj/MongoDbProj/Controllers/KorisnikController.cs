﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDbProj.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace MongoDbProj.Controllers
{
    public class KorisnikController : Controller
    {

        public IActionResult kreirajKorisnika()
        {
            Korisnik k = new Korisnik();
            return View(k);
        }

       
        public IActionResult kreirajKorisnikaa(Korisnik k)
        {
            
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var korisnikCollection = database.GetCollection<Korisnik>("korisnici");
            var prodavniceCollection = database.GetCollection<Prodavnica>("prodavnice");

            var nameFilter = Builders<Korisnik>.Filter.Eq("Password", k.Password) & Builders<Korisnik>.Filter.Eq("Email", k.Email);
            var korisnik = korisnikCollection.Find(nameFilter).FirstOrDefault();
            
            if(korisnik!=null)
            {
                TempData["idKorisnika"] = JsonConvert.SerializeObject(korisnik.Id.ToString());
                TempData["idKorpe"] = JsonConvert.SerializeObject(korisnik.Korpa.ToString());


            }
            else
            {
                //dodajemo u bazu

                //generisali kljuc korsisnika
                k.Id= ObjectId.GenerateNewId();

                //kreirali korpu
                Korpa korpa = kreirajKorpu(k.Id.ToString());//saljemo id korisnika
                k.Korpa = korpa.Id;
              
                korisnikCollection.InsertOne(k);

                TempData["idKorisnika"] = JsonConvert.SerializeObject(k.Id.ToString());
                TempData["idKorpe"] = JsonConvert.SerializeObject(k.Korpa.ToString());
            }
           
            //lista prodavnica
            var prod = prodavniceCollection.Find(_ => true).ToList();
            List<Prodavnica> l = new List<Prodavnica>();

            foreach (Prodavnica r in prod.ToArray<Prodavnica>())
            {
                l.Add(r);
            }
            

            // TempData["idKorisnika"] = k.Id.ToString();
            return View(l);
                   
        }
        public Korpa kreirajKorpu(string Id)//id korisnika
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korpaCollection = database.GetCollection<Korpa>("korpe");

            Korpa k = new Korpa();
            k.Id = ObjectId.GenerateNewId();
            k.Korisnik = ObjectId.Parse(Id); //id korisnika=Korisnik
            korpaCollection.InsertOne(k);

            return k;

        }
        public IActionResult dodajProizvodUkorpu(string Id)//id proizvoda
        {

            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korpaCollection = database.GetCollection<Korpa>("korpe");
            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var korisnikCollection = database.GetCollection<Korisnik>("korisnici");

            string idkorisnika = JsonConvert.DeserializeObject<string>(TempData["idKorisnika"] as string);
            TempData["idKorisnika"] = JsonConvert.SerializeObject(idkorisnika);

            //proizvod
            var idProiz = ObjectId.Parse(Id);
            var filter = Builders<Proizvod>.Filter.Eq("_id", idProiz);
            var proizvod = proizvodCollection.Find(filter).FirstOrDefault();
           
            //korisnik 
            var objId = ObjectId.Parse(idkorisnika);
            var nameFilter = Builders<Korisnik>.Filter.Eq("_id", objId);
            var korisnik = korisnikCollection.Find(nameFilter).FirstOrDefault();
           //korpa
            ObjectId idKorpe = korisnik.Korpa;
            var fil = Builders<Korpa>.Filter.Eq("_id", idKorpe);
            var korpa = korpaCollection.Find(fil).FirstOrDefault();
         
            if (proizvod.Kolicina>0)
            {
                int novaKolicina=proizvod.Kolicina-1;
                var update = Builders<Proizvod>.Update.Set("Kolicina", novaKolicina);
                proizvodCollection.UpdateOne(filter, update);

                korpa.Proizvodi.Add(proizvod);
                korpa.UkupnaCenaKorpe = korpa.UkupnaCenaKorpe + proizvod.Cena;
                var up = Builders<Korpa>.Update.Set("Proizvodi", korpa.Proizvodi).Set("UkupnaCenaKorpe", korpa.UkupnaCenaKorpe);
                korpaCollection.UpdateOne(fil, up);

            }
            //prikazi korpu
            return RedirectToAction("prikaziSadrzajKorpe");


        }
        public Korpa vratiKorpu(string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korpaCollection = database.GetCollection<Korpa>("korpe");

            var objId = ObjectId.Parse(Id);
            var fil = Builders<Korpa>.Filter.Eq("_id", objId);
            var korpa = korpaCollection.Find(fil).FirstOrDefault();
            return korpa;
        }
          public IActionResult prikaziSadrzajKorpe()
        {
            string Id = JsonConvert.DeserializeObject<string>(TempData["idKorpe"] as string);
            TempData["idKorpe"] = JsonConvert.SerializeObject(Id);


            var korpa = vratiKorpu(Id);

            return View(korpa);
        }
        public IActionResult obrisiProizvodIzKorpe(string Id,string  IdKorpe)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var korpaCollection = database.GetCollection<Korpa>("korpe");

            //korpa
            var korpa = vratiKorpu(IdKorpe);
           
            //proizvod koji se  brise iz korpe
            var objId = ObjectId.Parse(Id);
            var filt = Builders<Proizvod>.Filter.Eq("_id", objId);
            var proiz = proizvodCollection.Find(filt).FirstOrDefault();

            foreach(Proizvod p in korpa.Proizvodi)
            {
                if(p.Id==proiz.Id)
                {
                    //povecavamo kolicinu proizvoda, jer ga brisemo iz korpe 
                    proiz.Kolicina = proiz.Kolicina + 1;
                 
                    //cuvamo izmene prozvoda
                    var update = Builders<Proizvod>.Update.Set("Kolicina", proiz.Kolicina);
                    proizvodCollection.UpdateOne(filt, update);

                    //cuvamo izmene korpe
                    korpa.Proizvodi.Remove(p);
                    korpa.UkupnaCenaKorpe = korpa.UkupnaCenaKorpe - proiz.Cena;
                    var fil = Builders<Korpa>.Filter.Eq("_id", korpa.Id);
                    var up = Builders<Korpa>.Update.Set("Proizvodi", korpa.Proizvodi).Set("UkupnaCenaKorpe", korpa.UkupnaCenaKorpe);
                    korpaCollection.UpdateOne(fil, up);
                    return RedirectToAction("prikaziSadrzajKorpe");


                }
            }
            return RedirectToAction("prikaziSadrzajKorpe");
        }
        public IActionResult prikaziProizvodeKorisniku(string Id)
        {
            
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var prodavnicaCollection = database.GetCollection<Prodavnica>("prodavnice");
            TempData["idProdavnice"] = JsonConvert.SerializeObject(Id);


            var objId = ObjectId.Parse(Id);
            var nameFilter = Builders<Prodavnica>.Filter.Eq("_id", objId);
            var prod = prodavnicaCollection.Find(nameFilter).FirstOrDefault();

            List<Proizvod> lista = new List<Proizvod>();

            foreach (ObjectId idd in prod.proizvodi)
            {
                
                var filt = Builders<Proizvod>.Filter.Eq("_id", idd);
                var proiz = proizvodCollection.Find(filt).FirstOrDefault();

                lista.Add(proiz);

            }

           
            return View(lista);

        }

        public IActionResult prikaziKorisnike()
        {

            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korisniciCollection = database.GetCollection<Korisnik>("korisnici");




            var prod = korisniciCollection.Find(_ => true).ToList();
            List<Korisnik> k = new List<Korisnik>();

            foreach (Korisnik r in prod.ToArray<Korisnik>())
            {
                k.Add(r);
            }
            return View(k);
        }
        
        public IActionResult obrisiKorisnika(string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korisniciCollection = database.GetCollection<Korisnik>("korisnici");
            var korpaCollection = database.GetCollection<Korpa>("korpe");

            //korisnik
            var objId = ObjectId.Parse(Id);
            var filter = Builders<Korisnik>.Filter.Eq("_id", objId);
            var prozvod = korisniciCollection.Find(filter).FirstOrDefault();


            
            //brisanje korpe korisnika
            var filter1 = Builders<Korpa>.Filter.Eq("_id", prozvod.Korpa);
            korpaCollection.DeleteOne(filter1);
            //brisanje korinika
            var rez = korisniciCollection.DeleteOne(filter);



            return RedirectToAction("prikaziKorisnike");

        }
        public IActionResult idiNaIzmeniKorisnik(string Id)
        {
          
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korisniciCollection = database.GetCollection<Korisnik>("korisnici");

            var objId = ObjectId.Parse(Id);
            var nameFilter = Builders<Korisnik>.Filter.Eq("_id", objId);
            var k = korisniciCollection.Find(nameFilter).FirstOrDefault();

           
            return View(k);
        }
        public IActionResult izmeniKorisnika(Korisnik k,string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
            var korisniciCollection = database.GetCollection<Korisnik>("korisnici");

            var objId = ObjectId.Parse(Id);
            k.Id = objId;
            var nameFilter = Builders<Korisnik>.Filter.Eq("_id", objId);
            var update = Builders<Korisnik>.Update.Set("Email", k.Email).Set("Password", k.Password);
             

            var rez = korisniciCollection.UpdateOne(nameFilter, update);

            return RedirectToAction("prikaziKorisnike");

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDbProj.Models;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Mvc.Localization;
using Newtonsoft.Json;

namespace MongoDbProj.Controllers
{
    public class ProizvodController : Controller
    {
        public IActionResult idiNaKreirajProizvod(string Id) //treba sifra prodavnice
        {
            
            Proizvod p = new Proizvod();
            
            p.IdProdavnice = Id;
            return View(p);
        }
        public IActionResult kreirajPoizvod(Proizvod p)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");
          

            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var prodavnicaCollection = database.GetCollection<Prodavnica>("prodavnice");

            var idProd = ObjectId.Parse(p.IdProdavnice);
            var nameFilter = Builders<Prodavnica>.Filter.Eq("_id", idProd);

            var prod = prodavnicaCollection.Find(nameFilter).FirstOrDefault();

            if(prod!= null)
            {
                p.Id = ObjectId.GenerateNewId();
                prod.proizvodi.Add(p.Id);
                p.prodavnica = prod.Id;
                var update = Builders<Prodavnica>.Update.Set("proizvodi", prod.proizvodi);
                prodavnicaCollection.UpdateOne(nameFilter, update);
                proizvodCollection.InsertOne(p);
            }

            return RedirectToAction("prikaziProizvode", new { Id = p.IdProdavnice });
        }

        //vraca proizvod na osnovu id
        public Proizvod vratiProizvod(string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");

            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            
            var objId = ObjectId.Parse(Id);
            var nameFilter = Builders<Proizvod>.Filter.Eq("_id", objId);
            var proiz = proizvodCollection.Find(nameFilter).FirstOrDefault();

    
            return proiz;
            

        }


        public IActionResult prikaziProizvode(string Id)
        {

            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var prodavnicaCollection = database.GetCollection<Prodavnica>("prodavnice");


            var objId = ObjectId.Parse(Id);
            var nameFilter = Builders<Prodavnica>.Filter.Eq("_id", objId);
            var prod = prodavnicaCollection.Find(nameFilter).FirstOrDefault();

            List<Proizvod> lista = new List<Proizvod>();

            foreach ( ObjectId idd in prod.proizvodi)
            {
                string i = idd.ToString();
                var proiz = vratiProizvod(i);
                lista.Add(proiz);
                
            }


            return View(lista);
        } 
        public IActionResult obrisiProizvod(string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var prodavnicaCollection = database.GetCollection<Prodavnica>("prodavnice");

            //id proizvoda
            var objId = ObjectId.Parse(Id);
            //proizvod
            var filter = Builders<Proizvod>.Filter.Eq("_id", objId);
            var proizvod = proizvodCollection.Find(filter).FirstOrDefault();

            //prodavnica
            var idd = ObjectId.Parse(proizvod.IdProdavnice);
            var filter1 = Builders<Prodavnica>.Filter.Eq("_id", idd);
            var prodav = prodavnicaCollection.Find(filter1).FirstOrDefault();
            prodav.proizvodi.Remove(objId);//uklanjamo iz liste
            var update = Builders<Prodavnica>.Update.Set("proizvodi", prodav.proizvodi);
            prodavnicaCollection.UpdateOne(filter1, update);

            var rez = proizvodCollection.DeleteOne(filter);
           


            return RedirectToAction("prikaziProizvode", new { Id =proizvod.IdProdavnice });


        }
        public IActionResult idiNaIzmeniProizvod(string Id)
        {
            var objId = ObjectId.Parse(Id);
            
            Proizvod p = vratiProizvod(Id);
            

            return View(p);
        }

        //!!!1!! pravi problem putnanja nakon izmene, kada zelimo sl izmenu de na prikazi 
        public IActionResult izmeniProizvod(Proizvod p,string Id)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var prodavnicaCollection = database.GetCollection<Prodavnica>("prodavnice");

            
            var objId = ObjectId.Parse(Id);
            var nameFilter = Builders<Proizvod>.Filter.Eq("_id", objId);
           
            var update = Builders<Proizvod>.Update.Set("Sifra", p.Sifra).Set("_id", objId)
                .Set("Naziv", p.Naziv).Set("Cena", p.Cena).Set("Kolicina", p.Kolicina);

            var rez=proizvodCollection.UpdateOne(nameFilter, update);
            return RedirectToAction("prikaziProizvode", new { Id=p.IdProdavnice });
        }
        public IActionResult idiNaPretraziProizvod( )
        {
            return View();
        }
        //salje rez pretrage za prikaz
        public IActionResult pretraziProizvod(string Naziv)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("Ishop");


            var proizvodCollection = database.GetCollection<Proizvod>("proizvodi");
            var nameFilter = Builders<Proizvod>.Filter.Eq("Naziv", Naziv);
            var proiz = proizvodCollection.Find(nameFilter).ToList();
            List<Proizvod> lista = new List<Proizvod>();

            foreach (Proizvod pp in proiz)
            {
                lista.Add(pp);
            }
          
            return View(lista);


        }
    }
}